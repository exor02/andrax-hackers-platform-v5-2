package com.thecrackertechnology.dragonterminal.frontend.config

import com.thecrackertechnology.andrax.AndraxApp

/**
 * @author kiva
 */
object DefaultValues {

    val fontSize = 16

    val enableBell = true
    val enableVibrate = true
    val enableExecveWrapper = false
    val enableAutoCompletion = false
    val enableFullScreen = true
    val enableAutoHideToolbar = true
    val enableSwitchNextTab = false
    val enableExtraKeys = true
    val enableExplicitExtraKeysWeight = false
    val enableBackButtonBeMappedToEscape = false
    val enableSpecialVolumeKeys = false
    val enableWordBasedIme = false

    val loginShell = "andraxshell.sh"
    val initialCommand = ""
    val defaultFont = "SourceCodePro"

}
